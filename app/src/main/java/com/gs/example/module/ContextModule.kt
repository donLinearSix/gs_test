package com.gs.example.module

import android.content.Context
import com.gs.example.qualifier.ContextQualifier
import com.gs.example.scope.NetworkComponentScope
import dagger.Module
import dagger.Provides


@ContextQualifier
@Module
class ContextModule {

    private val context: Context

    constructor(context: Context) {
        this.context = context
    }


    @Provides
    @NetworkComponentScope
    fun getContext(): Context {
        return context
    }
}