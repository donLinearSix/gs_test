package com.gs.example.module

import android.content.Context
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.gs.example.BuildConfig
import com.gs.example.api.APIs
import com.gs.example.scope.NetworkComponentScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

@Module(includes = [NetworkModule::class])
class APIModule {
    @Provides
    @NetworkComponentScope
    fun getAPIService(retrofit: Retrofit): APIs {

        return retrofit.create<APIs>(APIs::class.java!!)
    }

    @Provides
    @NetworkComponentScope
    fun getGson(): Gson {

        val gsonBuilder = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setDateFormat("E, dd MMM yyyy HH:mm:ss Z")
            .serializeNulls()
        return gsonBuilder.create()
    }

    @Provides
    @NetworkComponentScope
    fun getRetrofit(client: OkHttpClient, gson: Gson, context: Context): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}