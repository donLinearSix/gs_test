package com.gs.example.module

import android.content.Context
import com.gs.example.scope.NetworkComponentScope
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit

@Module(includes = [ContextModule::class])
class NetworkModule {


    val interceptor: HttpLoggingInterceptor
        @Provides
        @NetworkComponentScope
        get() {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY


            return interceptor
        }

    @Provides
    @NetworkComponentScope
    fun getCacheFile(context: Context): File {
        return File(context.cacheDir, "okhttp.cache")
    }


    @Provides
    @NetworkComponentScope
    fun getCache(cacheFile: File): Cache {

        return Cache(cacheFile, (10 * 1000 * 1000).toLong())//10MG cache
    }


    @Provides
    @NetworkComponentScope
    fun getClient(
        loggingInterceptor: HttpLoggingInterceptor, cache: Cache,
        context: Context
    ): OkHttpClient {


        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .cache(cache)
            .build()
    }




}