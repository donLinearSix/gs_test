package com.gs.example.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.gs.example.MainApplication
import com.gs.example.model.JobsResponse
import com.gs.example.utils.Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class JobRepository(val application: MainApplication) {

    /**
     * get job list form API
     * User RXJava for reactive approach
     */
    fun getJobList(date: String) : Observable<JobsResponse> {
        return  application!!.apiService!!.getData(date)
    }
}