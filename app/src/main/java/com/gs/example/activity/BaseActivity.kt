package com.gs.example.activity

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.gs.example.MainApplication
import com.gs.example.R

abstract class BaseActivity : AppCompatActivity() {


    private var dialogProgressbar: Dialog? = null

    var context: Context? = null
    var application: MainApplication? = null


    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        application = MainApplication.instance
        application!!.appComponent!!.inject(this)
        context = this

    }




    protected fun showProgressBar(context: Context, text: String) {
        try {
            if (dialogProgressbar != null) {
                try {
                    dialogProgressbar!!.dismiss()
                } catch (e: Exception) {
                }

                dialogProgressbar = null
            }
            dialogProgressbar = Dialog(context)
            dialogProgressbar?.setCancelable(false)
            dialogProgressbar?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialogProgressbar?.setContentView(R.layout.progress_bar)
            val progressbarTextView = dialogProgressbar?.findViewById(R.id.progressbarTextView) as TextView
            if (text.trim { it <= ' ' }.isEmpty()) {
                progressbarTextView.setVisibility(View.GONE)
            } else {
                progressbarTextView.setVisibility(View.VISIBLE)
                progressbarTextView.setText(text)
            }
            dialogProgressbar?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * hide progress bar
     */
    fun hideProgressDialog() {
        try {
            if (dialogProgressbar != null) {
                dialogProgressbar!!.dismiss()
                dialogProgressbar = null
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


//    protected fun callHome() {
//        drawerLayout!!.closeDrawer(GravityCompat.START)
//        val callHome = Intent(context, HomeMapsActivity::class.java)
//        callHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)//| Intent.FLAG_ACTIVITY_NEW_TASK
//        startActivity(callHome)
//    }
}