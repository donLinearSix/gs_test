package com.gs.example.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.gs.example.MainApplication
import com.gs.example.R
import com.gs.example.action.DialogType
import com.gs.example.adapter.JobsListAdapter
import com.gs.example.databinding.ActivityMainBinding
import com.gs.example.model.JobsItem
import com.gs.example.model.JobsResponse
import com.gs.example.mv.HomeMv
import com.gs.example.ui.CommonAlert
import com.gs.example.utils.InternetConnection
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList



class MainActivity : BaseActivity() {


    private var jobsDisposable: CompositeDisposable? = null
    private var mainViewModel: HomeMv? = null
    var jobList: ArrayList<JobsItem>? = ArrayList()
    private var adapter: JobsListAdapter? = null
    private val date = Date()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainApplication.instance!!.appComponent!!.inject(this)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_main
        ) as ActivityMainBinding

        binding.viewModel = mainViewModel;
        init()

        if (InternetConnection.isConnectingToInternet(this)) {
            showProgressBar(this, "Loading...")

            mainViewModel!!.getJobList(getDate()).observe(this, Observer { jobData ->

                hideProgressDialog()
                var response: JobsResponse = jobData!!.posts as JobsResponse
                for (key in response.data!!.keys) {
                    response.data?.get(key)?.let { jobList?.addAll(it) }
                }
                adapter?.setData(jobList!!)

            })


        } else {
            CommonAlert.showSnackBar(
                this, rootView, resources.getString(R.string.no_internet),
                Snackbar.LENGTH_LONG, DialogType.ERROR, R.dimen.snack_margin_main_top
            )
        }


    }


    /**
     * initialize component
     */
    private fun init() {
        jobsDisposable = CompositeDisposable()
        mainViewModel = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return HomeMv(application = MainApplication.instance!!) as T
            }
        }).get(HomeMv::class.java)


        adapter = JobsListAdapter(jobList!!, this)


        jobsRecyclerView.layoutManager = LinearLayoutManager(this)
        jobsRecyclerView.adapter = adapter;



    }

    fun getDate(): String {
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        return formatter.format(date)
    }

    override fun onDestroy() {
        super.onDestroy()
    }


}
