package com.gs.example.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gs.example.R
import com.gs.example.databinding.ViewJobRowBinding
import com.gs.example.model.JobsItem
import com.squareup.picasso.Picasso


class JobsListAdapter(private var jobList: ArrayList<JobsItem>, val context: Context) :
    RecyclerView.Adapter<JobsListAdapter.ViewHolder>() {


    class ViewHolder(private val binding: ViewJobRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: JobsItem) {
            binding.jobItem = item
            binding.executePendingBindings()
        }

    }


    companion object {
        @BindingAdapter("app:jobImage")
        @JvmStatic
        fun ImageView.loadImage(imageUrl: String?) {
            Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.img_empty)
                .error(R.drawable.img_empty)
                .into(this)
        }

        @BindingAdapter("app:shiftData")
        @JvmStatic
        fun TextView.loadIShift(item: JobsItem?) {
            val sb = StringBuilder()
            for (shift in item!!.shifts!!) {
                sb.append(shift.start_time).append(" ").append(shift.end_time).append("\n")
                break

            }
            this.text = sb.toString()

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewJobRowBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(jobList[position])


    internal fun setData(jobListAdd: ArrayList<JobsItem>) {
        this.jobList = jobListAdd
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return jobList.size
    }
}