package com.gs.example.mv

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gs.example.MainApplication
import com.gs.example.model.ErrorResponse
import com.gs.example.model.JobsResponse
import com.gs.example.repository.JobRepository
import com.gs.example.utils.Utils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeMv(val application: MainApplication) : ViewModel() {

    private var mutableLiveData = MutableLiveData<ErrorResponse>()

    /**
     * get job list from Repo
     */
    fun getJobList(date: String) : MutableLiveData<ErrorResponse> {
        var repo = JobRepository(application)


        repo.getJobList(date)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext{
            }
            .subscribe({
                var response: JobsResponse = it
                mutableLiveData.postValue(ErrorResponse(response))

            }, {
                it.printStackTrace()
                mutableLiveData.postValue(ErrorResponse(it))

            })

        return mutableLiveData;
    }


    fun onSignUpClick() {
        Log.e("ERROR", "::::::::CLCIKK")
    }
}