package com.gs.example.component

import com.gs.example.MainApplication
import com.gs.example.activity.BaseActivity
import com.gs.example.activity.MainActivity
import com.gs.example.api.APIs
import com.gs.example.module.APIModule
import com.gs.example.scope.NetworkComponentScope
import dagger.Component

@NetworkComponentScope
@Component(modules = [APIModule::class])
interface AppComponent {


    abstract fun getAPIService(): APIs
    fun inject(application: MainApplication)


    fun inject(activity: MainActivity)
    fun inject(activity: BaseActivity)


}