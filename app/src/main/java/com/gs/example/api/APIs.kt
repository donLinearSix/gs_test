package com.gs.example.api

import com.gs.example.model.JobsResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface APIs {

    @GET("shifts?")
    abstract fun getData(@Query("dates") lat: String): Observable<JobsResponse>

}