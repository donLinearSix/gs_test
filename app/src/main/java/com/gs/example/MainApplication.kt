package com.gs.example

import android.app.Application
import com.gs.example.api.APIs
import com.gs.example.component.AppComponent
import com.gs.example.component.DaggerAppComponent
import com.gs.example.module.APIModule
import com.gs.example.module.ContextModule
import com.gs.example.module.NetworkModule

class MainApplication : Application() {

    var appComponent: AppComponent? = null
        private set


    val apiService: APIs
        get() = appComponent!!.getAPIService()

    override fun onCreate() {
        super.onCreate()

        instance = this

        appComponent = DaggerAppComponent.builder()
            .aPIModule(APIModule())
            .networkModule(NetworkModule())
            .contextModule(ContextModule(this))
            .build()
        appComponent!!.inject(this)

    }

    companion object {
        var instance: MainApplication? = null
            private set
    }
}