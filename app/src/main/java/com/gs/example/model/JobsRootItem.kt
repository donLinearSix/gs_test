package com.gs.example.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.HashMap

class JobsRootItem {

    @SerializedName("data")
    @Expose
    private var data: HashMap<String, JobsItem>? = null
}