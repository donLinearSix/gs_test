package com.gs.example.model

import com.google.gson.annotations.SerializedName


class JobsItem : BaseModel {
    @SerializedName("title")
    var title: String? = null

    @SerializedName("photo")
    var photo: String? = null

    @SerializedName("max_possible_earnings_hour")
    var max_possible_earnings_hour: Double? = null

    @SerializedName("job_category")
    var job_category: JobCategory? = null

    @SerializedName("shifts")
    var shifts: ArrayList<Shifts>? = null


    class Shifts {
        @SerializedName("start_time")
        var start_time: String? = null
        @SerializedName("end_time")
        var end_time: String? = null
        @SerializedName("currency")
        var currency: String? = null
    }

    class JobCategory {

        @SerializedName("description")
        var description: String? = null
        @SerializedName("icon_path")
        var icon_path: String? = null
        @SerializedName("slug")
        var slug: String? = null

    }

}