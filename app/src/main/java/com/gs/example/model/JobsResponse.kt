package com.gs.example.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class JobsResponse : BaseModel {
    @SerializedName("data")
    @Expose
    var data: HashMap<String, ArrayList<JobsItem>>? = null

}