package com.gs.example.listener

interface AlertButtonClickListener {
    abstract fun onAlertButtonClick()
}