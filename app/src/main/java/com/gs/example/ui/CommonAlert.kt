package com.gs.example.ui

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.FrameLayout
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.gs.example.R
import com.gs.example.action.DialogType
import com.gs.example.listener.AlertButtonClickListener

object CommonAlert {

    /**
     * Show snack bar
     */
    fun showSnackBar(
        context: Context, rootView: View, msg: String, type: Int,
        dialogType: DialogType, margin: Int
    ) {
        val snack = Snackbar
            .make(
                rootView, msg,
                type
            )
        val view = snack.getView()
        val params = view.getLayoutParams() as FrameLayout.LayoutParams
        params.gravity = Gravity.TOP
        params.topMargin = context.resources.getDimension(margin).toInt()


        if (dialogType === DialogType.ERROR) {
            view.setBackgroundColor(context.resources.getColor(R.color.error_red, null))
        } else {
            view.setBackgroundColor(context.resources.getColor(R.color.success_green, null))
        }

        snack.show()
    }

}