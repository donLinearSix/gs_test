package com.gs.example.utils

import android.util.Log
import com.gs.example.BuildConfig

public class Utils {

    /**
     * show error log - only in debug code
     */
    companion object {
        fun log(tag: String, msg: String) {
            if (BuildConfig.DEBUG) {
                Log.e(tag, msg)
            }
        }
    }

    /**
     * show error log with type - only in debug code
     */
    fun log(tag: String, msg: String, type: Int) {
        if (BuildConfig.DEBUG) {
            when (type) {
                Log.ASSERT -> {
                }
                Log.DEBUG -> Log.d(tag, msg)
                Log.ERROR -> Log.e(tag, msg)
                Log.WARN -> Log.w(tag, msg)
                Log.INFO -> Log.i(tag, msg)
                Log.VERBOSE -> Log.v(tag, msg)
            }


        }
    }
}