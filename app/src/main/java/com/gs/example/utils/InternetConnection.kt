package com.gs.example.utils

import android.content.Context
import android.net.ConnectivityManager

object InternetConnection {


    /**
     * check Internet connection available in system
     */
    fun isConnectingToInternet(context: Context): Boolean {
        try {
            val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivity != null) {
                val netInfo = connectivity.activeNetworkInfo
                if (netInfo != null && netInfo.isConnectedOrConnecting) {
                    return true
                }
            }
            return false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }

}