package com.gs.example.scope;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Created by ayesh on 2/20/17.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface NetworkComponentScope {
}
